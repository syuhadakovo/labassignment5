function final()
{
	var name = document.forms["myForm"]["name"].value;
	var length = document.forms["myForm"]["length"].value;
	var width = document.forms["myForm"]["width"].value;
	var height = document.forms["myForm"]["height"].value;
	var mode = document.forms["myForm"]["mode"].value;
	var type = document.forms["myForm"]["type"].value;
	var weight;
	var cost;

	weight = (length*width*height)/5000;

	alert("Parcel Volumetric and Cost Calculator"+"\nCustomer name: "+upperCase(name)+"\nLength: "+length+"cm"+"\nWidth: "+width+"cm"+"\nHeight: "+height+"cm"+"\nWeight: "+weight+"kg"+"\nMode: "+mode+"\nType: "+type+"\nDelivery cost: RM"+calculateCostParcel(type,weight,mode));
}

function msgreset()
{
	alert("The input will be reset");
}

function upperCase(name)
{
	str = name.toUpperCase();
	return str;
}


function calculateCostParcel(type,weight,mode)
{
	var cost;
	if(type=="Domestic")
	{
		if(weight<2.00)
		{
			if(mode=="Surface")
			{
				cost=7;
			}
			else if(mode=="Air")
			{
				cost=10;
			}
		}
		else
		{
			if(mode=="Surface")
			{
				cost=7+((weight-2.00)*1.5);
			}
			else if(mode=="Air")
			{
				cost=10+((weight-2.00)*3);
			}
		}
	}
	else if(type=="International")
	{
		if(weight<2.00)
		{
			if(mode=="Surface")
			{
				cost=20;
			}
			else if(mode=="Air")
			{
				cost=50;
			}
		}
		else
		{
			if(mode=="Surface")
			{
				cost=20+((weight-2.00)*3);
			}
			else if(mode=="Air")
			{
				cost=50+((weight-2.00)*5);
			}
		}
	}
	return cost;
}